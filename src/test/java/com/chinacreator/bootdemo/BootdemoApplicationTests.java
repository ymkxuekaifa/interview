package com.chinacreator.bootdemo;

import com.ymk.BootDemoApplication;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.junit.FixMethodOrder;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.IOException;
import java.util.Objects;

@Slf4j
@RunWith(SpringRunner.class)
@FixMethodOrder(MethodSorters.JVM)
@SpringBootTest(classes = BootDemoApplication.class)
class BootdemoApplicationTests {

    @Test
    void contextLoads1() {
        System.out.println("studentService = ");
    }

    @Test
    void demo2() {

        boolean result = Objects.equals(null, null);
        System.out.println("result = " + result);
        boolean strUResult = StringUtils.equals(null, null);
        System.out.println("strUResult = " + strUResult);

        log.error("bad sql");
        // System.out.println("studentService = " + studentService.findAll());
    }


    @Test
    void demo3() throws IOException {
        File file = new File(this.getClass().getResource("").getPath());

        String rootPath = file.getPath();
        String rootPath1 = file.getAbsolutePath();
        String rootPath2 = file.getCanonicalPath();
        String rootPath3 = file.toURI().toString();


        System.out.println("rootPath = " + rootPath);
        System.out.println("rootPath1 = " + rootPath1);
        System.out.println("rootPath2 = " + rootPath2);
        System.out.println("rootPath3 = " + rootPath3);

        String rootPath4 = rootPath.substring(rootPath.indexOf("main"), rootPath.indexOf("src"));
        System.out.println("rootPath4 = " + rootPath4);

    }


}
