package com.ymk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.concurrent.ConcurrentHashMap;

/**
 * @Author : ymk
 * @Package: com.ymk.facetest
 * @Description:  启动类
 * @Date: 2021/10/11
 */
@SpringBootApplication
public class BootDemoApplication {
    public static void main(String[] args) {

        SpringApplication.run(BootDemoApplication.class);
    }
}
